FROM registry.access.redhat.com/ubi8/python-38

ENV PIPENV_IGNORE_VIRTUALENVS=1

ADD Pipfile* *.py uwsgi.ini ${HOME}

RUN pip install pipenv && pipenv install

EXPOSE 8080

USER 1001

CMD ["pipenv", "run", "uwsgi", "--ini", "uwsgi.ini"]

